package com.etc;

import org.apache.hadoop.conf.Configuration;
import org.apache.hadoop.fs.*;
import org.junit.Before;
import org.junit.Test;

import java.io.BufferedReader;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.URI;
import java.net.URISyntaxException;
import java.sql.SQLOutput;
import java.util.Arrays;

public class HdfsTest {

    FileSystem fs = null;

    /**
     * 获得文件系统
     *
     * @return fileSystem
     */
    @Before
    public void getFileSystem() throws URISyntaxException, IOException, InterruptedException {
        /**
         * configuration参数对象的机制：构造时，会加载jar包中的默认配置 xx-defaul。xmt 在加载
         * 用户配置xx-site.xml ，覆盖掉默认参数 构造完成之后，还可以conf.set("p","v")，会再次覆盖用户配置文件中的参数值
         */
        // new Configuration()会从项目的classpath中加载core-default.xml hdfs-default.xml
        // core-site.xml hdfs-site.xml等文件
        Configuration conf = new Configuration();
        // 指定本客户端上传文件到hdfs时需要保存的副本数为：2
        conf.set("dfs.replication", "2");
        // 指定本客户端上传文件到hdfs时切块的规格大小：64M
        conf.set("dfs.blocksize", "64m");
        // 构造一个访问指定HDFS系统的客户端对象:
        // 参数1:——HDFS系统的URI，参数2：——客户端要特别指定的参数，参数3：客户端的身份（用户名）
        fs = FileSystem.get(new URI("hdfs://Master:9000"), conf, "root");
    }

    /*
    上传文件到sdfs
    @throws IOException
     */
    @Test
    public void copyFileToHdfs() throws IOException {
        //源文件路径是Linux下的路径，如果早Windows下测试，需要改写Windows下的路径，比如比如D://hadoop/djt/weibo.txt
        Path srcPath = new Path("f:\\b.txt");
        //目的路径
        Path hdfsUri = new Path("/aaa");

        fs.copyFromLocalFile(srcPath, hdfsUri);
        fs.close();
    }

    /*创建文件夹
    @ throws IOException
    @ throws URISyntaxException
    @ throws InterrupteException
     */
    @Test
    public void mkdir() throws IOException, URISyntaxException, InterruptedException {
        fs.mkdirs(new Path("/input"));
        fs.close();
    }

    /*
    在hdfs中删除文件或者文件目录
    @ throws IOException
     */
    @Test
    public void remove() throws IOException {
        fs.delete(new Path("/input"), true);
        fs.close();
    }

    /**
     * 在hdfs内部移动文件\修改名称
     */
    @Test
    public void testRename() throws Exception {
        fs.rename(new Path("/aaa/b.txt"), new Path("/lxqss"));
        fs.close();
    }

    /**
     * 从HDFS中下载文件到客户端本地磁盘
     *
     * @throws IOException
     * @throws IllegalArgumentException
     */
    @Test
    public void testGet() throws  IOException ,IllegalArgumentException{
        fs.copyToLocalFile(false, new Path("/lxq/a.txt"), new Path("g:\\sdf.txt"), true);
        fs.close();
    }

    /*
    查询hdfs指定目录下的文件信息
     */
    @Test
    public void testLs() throws Exception {
        //只查询文件的信息，不返回文件夹的信息
        RemoteIterator<LocatedFileStatus> iter = fs.listFiles(new Path("/"), true);

        while (iter.hasNext()) {
            LocatedFileStatus status = iter.next();
            System.out.println("文件全路径：" + status.getPath());
            System.out.println("块大小：" + status.getBlockSize());
            System.out.println("文件长度: " + status.getLen());
            System.out.println("快信息：" + Arrays.toString(status.getBlockLocations()));


            System.out.println("----------------------------------");
        }
            fs.close();
    }

    /**
     * 查询hdfs指定目录下的文件和文件夹信息
     */
    @Test
    public void testLs2() throws Exception {
        FileStatus[] listStatus = fs.listStatus(new Path("/lxq/a.txt"));

        for (FileStatus status : listStatus) {
            System.out.println("文件全路径：" + status.getPath());
            System.out.println(status.isDirectory() ? "这是文件夹" : "这是文件");
            System.out.println("块大小：" + status.getBlockSize());
            System.out.println("文件长度：" + status.getLen());
            System.out.println("副本数量：" + status.getReplication());

            System.out.println("--------------------------------");
        }
        fs.close();
    }

    /**
     * 读取hdfs中的文件的内容
     *
     * @throws IOException
     * @throws IllegalArgumentException
     */
    @Test
    public void testReadData() throws IllegalArgumentException, IOException {

        FSDataInputStream in = fs.open(new Path("/lxq/a.txt"));

        BufferedReader br = new BufferedReader(new InputStreamReader(in, "utf-8"));

        String line = null;
        while ((line = br.readLine()) != null) {
            System.out.println(line);
        }

        br.close();
        in.close();
        fs.close();
    }

    /**
     * 往hdfs中的文件写内容
     *
     * @throws IOException
     * @throws IllegalArgumentException
     */

    @Test
    public void testWriteData() throws IllegalArgumentException, IOException {

        FSDataOutputStream out = fs.create(new Path("/lxq/c.txt"), false);
        FileInputStream in = new FileInputStream("f:/c.txt");

        byte[] buf = new byte[1024];
        int read = 0;
        while ((read = in.read(buf)) != -1) {
            out.write(buf, 0, read);
        }
        in.close();
        out.close();
        fs.close();
    }

    /**
     * 读取hdfs中文件的指定偏移量范围的内容
     * <p>
     * <p>
     * 作业题：用本例中的知识，实现读取一个文本文件中的指定BLOCK块中的所有数据
     *
     * @throws IOException
     * @throws IllegalArgumentException
     */
    @Test
    public void testRandomReadData() throws IllegalArgumentException, IOException {

        FSDataInputStream in = fs.open(new Path("/lxq/c.txt"));

        // 将读取的起始位置进行指定
        in.seek(12);

        // 读16个字节
        byte[] buf = new byte[16];
        in.read(buf);

        System.out.println(new String(buf));

        in.close();
        fs.close();

    }

}










